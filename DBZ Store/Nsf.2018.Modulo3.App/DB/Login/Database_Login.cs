﻿using MySql.Data.MySqlClient;
using Nsf._2018.Modulo3.App.DB.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.App.DB.Login
{
    class Database_Login
    {
        public bool Login (string login, string senha)
        {
            string script =
            @"SELECT * FROM tb_login
                WHERE nm_usuario = @nm_usuario
                   AND ds_senha = @ds_senha";

            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("nm_usuario", login));
            parm.Add(new MySqlParameter("ds_senha", senha));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parm);

            if (reader.Read())
            {
                return true;
            }
            else
            {
                return false;
            }

                    
        }

        public int Salvar (DTO_Login dto)
        {
            string script =
            @"INSERT INTO tb_login
            (
                nm_usuario,
                ds_email,
                ds_senha,
                ds_cpf
            )
            VALUES
            (
                @nm_usuario,
                @ds_email,
                @ds_senha,
                @ds_cpf
            )";

            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("nm_usuario", dto.Usuario));
            parm.Add(new MySqlParameter("ds_email", dto.Email));
            parm.Add(new MySqlParameter("ds_senha", dto.Senha));
            parm.Add(new MySqlParameter("ds_cpf", dto.CPF));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parm);
        }

        public void Alterar (DTO_Login dto)
        {
            string script =
                @"UPDATE tb_login
                    SET  nm_usuario  = @nm_usuario,
                         ds_email    = @ds_email,
                         ds_senha    = @ds_senha,
                         ds_cpf      = @ds_cpf
                    WHERE id_login = @id_login";

            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("id_login", dto.ID));
            parm.Add(new MySqlParameter("nm_usuario", dto.Usuario));
            parm.Add(new MySqlParameter("ds_email", dto.Email));
            parm.Add(new MySqlParameter("ds_senha", dto.Senha));
            parm.Add(new MySqlParameter("ds_cpf", dto.CPF));

            Database db = new Database();
            db.ExecuteInsertScript(script, parm);
        }
        


    }
}
