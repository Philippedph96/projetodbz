﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.App.DB.Login
{
    class Business_Login
    {
        public bool Logar (string login, string senha)
        {
            Database_Login db = new Database_Login();
            return db.Login(login, senha);
        }

        public int Salvar (DTO_Login dto)
        {
            if (dto.Usuario == string.Empty)
            {
                throw new ArgumentException("Usuario precisa ser definido");
            }
            if (dto.Email == string.Empty)
            {
                throw new ArgumentException("Email precisa ser definido da maneira correta");
            }
            if (dto.Senha == string.Empty)
            {
                throw new ArgumentException("Senha precisa ser definida da maneira correta");
            }
            if (dto.CPF == string.Empty)
            {
                throw new ArgumentException("Senha precisa ser definida da maneira correta");
            }

            Database_Login db = new Database_Login();
            return db.Salvar(dto);
        }

        public void Alterar (DTO_Login dto)
        {
            Database_Login db = new Database_Login();
            db.Alterar(dto);
        }
    }
}
