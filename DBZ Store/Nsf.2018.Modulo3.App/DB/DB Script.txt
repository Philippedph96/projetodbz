﻿create database DBZStoreDB;

use DBZStoreDB;

select * from tb_produto;
select * from tb_pedido;
select * from tb_pedido_item;
select * from vw_pedido_consultar;


-- CRIA A TABELA PRODUTO
create table tb_produto (
	id_produto int primary key auto_increment,
    nm_produto varchar(100),
    vl_preco decimal(15,2)
);


-- CRIA A TABELA PEDIDO
create table tb_pedido (
	id_pedido int primary key auto_increment,
    nm_cliente varchar(100),
    ds_cpf varchar(100),
    dt_venda datetime
);


-- CRIA A TABELA PEDIDO ITEM
create table tb_pedido_item (
	id_pedido_item int primary key auto_increment,
    id_pedido int,
    id_produto int,
    foreign key (id_pedido) references tb_pedido (id_pedido),
    foreign key (id_produto) references tb_produto (id_produto)
);


-- CRIA A VIEW PEDIDO CONSULTAR
create view vw_pedido_consultar as 
	select tb_pedido.id_pedido,
		   tb_pedido.nm_cliente,
           tb_pedido.dt_venda,
		   tb_pedido.ds_cpf,
		   count(tb_pedido_item.id_pedido_item) 	as qtd_itens,
           sum(tb_produto.vl_preco)		 			as vl_total
      from tb_pedido
      join tb_pedido_item
        on tb_pedido.id_pedido = tb_pedido_item.id_pedido
	  join tb_produto
        on tb_pedido_item.id_produto = tb_produto.id_produto
	 group 
	    by tb_pedido.id_pedido,
		   tb_pedido.nm_cliente,
		   tb_pedido.ds_cpf,
           tb_pedido.dt_venda;

drop view vw_pedido_consultar;

--Tabela de Cadastro 
create table tb_cadastro (
id_cadastro int primary key auto_increment,
nm_nome varchar(60) not null,
nm_usuario varchar(30) not null unique,
ds_email varchar(100) not null unique,
ds_senha varchar (14) not null ,
ds_cfp varchar(15) not null unique
);

-- TABELA LOGIN
create table tb_login(
id_login int primary key auto_increment,
nm_usuario varchar(30) not null unique,
ds_email varchar(100) unique,
ds_senha varchar (260) not null,
ds_cpf varchar(15) unique

);

DROP TABLE tb_login;
SELECT * FROM tb_login;
TRUNCATE TABLE tb_login;