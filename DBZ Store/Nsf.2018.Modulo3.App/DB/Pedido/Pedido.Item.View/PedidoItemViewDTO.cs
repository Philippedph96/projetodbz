﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.App.DB.Pedido.Item.View
{
    class ViewDTO
    {
        public int IDPedido { get; set; }
        public string Cliente{ get; set; }
        public DateTime Data { get; set; }
        public int IDPedidoItem { get; set; }
        public decimal Preco { get; set; }
        public string CPF { get; set; }
    }
}
