﻿using MySql.Data.MySqlClient;
using Nsf._2018.Modulo3.App.DB.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.App.DB.Pedido.Item
{
    class PedidoItemDatabase
    {
        public int Salvar (PedidoItemDTO dto)
        {
            string script =
            @"INSERT INTO tb_pedido_item
            (
                id_pedido,
                id_produto
            )
            VALUES
            (
                @id_pedido,
                @id_produto
            )";
            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("id_pedido", dto.IDPedido));
            parm.Add(new MySqlParameter("id_produto", dto.IDProduto));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parm);
        }
    }
}
