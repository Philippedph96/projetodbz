﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.App.DB.Pedido.Item
{
    class PedidoItemDTO
    {
        public int ID { get; set; }
        public int IDPedido{ get; set; }
        public int IDProduto { get; set; }
    }
}
