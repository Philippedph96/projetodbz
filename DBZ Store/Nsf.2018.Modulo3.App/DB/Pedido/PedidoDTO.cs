﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.App.DB.Pedido
{
    class PedidoDTO
    {
        public int ID { get; set; }
        public string Cliente { get; set; }
        public string CPF { get; set; }
        public DateTime Data { get; set; }
    }
}
