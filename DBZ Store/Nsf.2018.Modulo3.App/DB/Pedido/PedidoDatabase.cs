﻿using MySql.Data.MySqlClient;
using Nsf._2018.Modulo3.App.DB.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.App.DB.Pedido
{
    class PedidoDatabase
    {
        public int Salvar (PedidoDTO dto)
        {
            string script =
            @"INSERT INTO tb_pedido
            (
                nm_cliente,
                ds_cpf,
                dt_venda
            )
            VALUES
            (
                @nm_cliente,
                @ds_cpf,
                @dt_venda
            )";
            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("nm_cliente", dto.Cliente));
            parm.Add(new MySqlParameter("ds_cpf", dto.CPF));
            parm.Add(new MySqlParameter("dt_venda", dto.Data));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parm);
            
        }
        public void Remover(int id)
        {
            string script =
                @"DELETE FROM tb_pedido
                        WHERE id_pedido = @id_pedido";
            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("id_pedido", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parm);

        }
    }
}
