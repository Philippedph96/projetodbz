﻿using Nsf._2018.Modulo3.App.DB.Pedido.Item;
using Nsf._2018.Modulo3.App.DB.Produto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.App.DB.Pedido
{
    class PedidoBusiness
    {
        public int Salvar (PedidoDTO pedido, List<ProdutoDTO> produtos)
        {
            PedidoDatabase db = new PedidoDatabase();
            int idpedido = db.Salvar(pedido);

            PedidoItemBusiness business = new PedidoItemBusiness();
            foreach (ProdutoDTO item in produtos)
            {
                PedidoItemDTO dto = new PedidoItemDTO();
                dto.IDProduto = item.ID;
                dto.IDPedido = idpedido;

                business.Salvar(dto);
            }
            return idpedido;
        }
        public void Remover (int id)
        {
            PedidoDatabase db = new PedidoDatabase();
            db.Remover(id);
        }
    }
}
