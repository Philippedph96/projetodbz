﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.App.DB.Produto
{
    class ProdutoBusiness
    {
        public int Salvar (ProdutoDTO dto)
        {
            if (dto.Produto == string.Empty)
            {
                throw new ArgumentException("O nome do produto precisa ser preenchido");
            }
            if (dto.Preco == 0)
            {
                throw new ArgumentException("O valor do produto precisa ser preenchido");
            }

            ProdutoDatabase db = new ProdutoDatabase();
            return db.Salvar(dto);
        }

        public List<ProdutoDTO> Consultar (string produto)
        {
            ProdutoDatabase db = new ProdutoDatabase();
            List<ProdutoDTO> list = db.Consultar(produto);

            return list;
        }

        public List<ProdutoDTO> Listar()
        {
            ProdutoDatabase db = new ProdutoDatabase();
            List<ProdutoDTO> list = db.Listar();

            return list;
        }

    }
}
