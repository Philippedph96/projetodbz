﻿using MySql.Data.MySqlClient;
using Nsf._2018.Modulo3.App.DB.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.App.DB.Produto
{
    class ProdutoDatabase
    {
        public int Salvar (ProdutoDTO dto)
        {
            string script =
            @"INSERT INTO tb_produto
            (
                nm_produto,
                vl_preco
            )
            VALUES
            (
                @nm_produto,
                @vl_preco
            )";
            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("nm_produto", dto.Produto));
            parm.Add(new MySqlParameter("vl_preco", dto.Preco));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parm);
        }

        public List<ProdutoDTO> Consultar (string produto)
        {
            string script =
            @"SELECT * from tb_produto
                WHERE nm_produto like @nm_produto";

            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("nm_produto", "%" + produto + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parm);
            List<ProdutoDTO> fora = new List<ProdutoDTO>();
            while (reader.Read())
            {
                ProdutoDTO dentro = new ProdutoDTO();
                dentro.ID = reader.GetInt32("id_produto");
                dentro.Produto = reader.GetString("nm_produto");
                dentro.Preco = reader.GetDecimal("vl_preco");

                fora.Add(dentro);
            }
            reader.Close();
            return fora;
        }
        public List<ProdutoDTO> Listar()
        {
            string script =
            @"SELECT * from tb_produto";
            List<MySqlParameter> parm = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parm);
            List<ProdutoDTO> fora = new List<ProdutoDTO>();
            while (reader.Read())
            {
                ProdutoDTO dentro = new ProdutoDTO();
                dentro.ID = reader.GetInt32("id_produto");
                dentro.Produto = reader.GetString("nm_produto");
                dentro.Preco = reader.GetDecimal("vl_preco");

                fora.Add(dentro);
            }
            reader.Close();
            return fora;
        }

    }
}
