﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.App.DB.Produto
{
    class ProdutoDTO
    {
        public int ID { get; set; }
        public string Produto { get; set; }
        public decimal Preco { get; set; }
    }
}
