﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Nsf._2018.Modulo3.App.DB.Produto;
using Nsf._2018.Modulo3.App.DB.Pedido;
using System.Text.RegularExpressions;

namespace Nsf._2018.Modulo3.App.Telas
{
    public partial class frmPedidoCadastrar : UserControl
    {
        BindingList<ProdutoDTO> carrinho = new BindingList<ProdutoDTO>();

        public frmPedidoCadastrar()
        {
            InitializeComponent();
            CarregarCombos();
            ConfigurarGrid();
        }
        public void CarregarCombos()
        {
            ProdutoBusiness db = new ProdutoBusiness();
            List<ProdutoDTO> list = db.Listar();


            cboProduto.ValueMember = nameof(ProdutoDTO.ID);
            cboProduto.DisplayMember = nameof(ProdutoDTO.Produto);
            cboProduto.DataSource = list;
        }

        public void ConfigurarGrid()
        {
            dgvItens.AutoGenerateColumns = false;
            dgvItens.DataSource = carrinho;
        }
        
        private void btnEmitir_Click(object sender, EventArgs e)
        {
            PedidoDTO dto = new PedidoDTO();
            dto.Cliente = txtCliente.Text;
            dto.CPF = txtCpf.Text;
            dto.Data = DateTime.Now;

             string re = @"^\d\d\d[.]\d\d\d[.]\d\d\d[-]\d\d$";

            Regex regex = new Regex(re);

            if (regex.IsMatch(dto.CPF) == false)
            {
                MessageBox.Show("Não esqueça de digitar os sinais de pontuação",
                                "CPF Invalido",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Exclamation);
                return;
            }


            PedidoBusiness db = new PedidoBusiness();
            db.Salvar(dto, carrinho.ToList());
            MessageBox.Show("Produto salvo com sucesso",
                            "Sucesso!!",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Information);
            this.Hide();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            ProdutoDTO dto = cboProduto.SelectedItem as ProdutoDTO;
            for (int i = 0; i < int.Parse(txtQuantidade.Text); i++)
            {
                carrinho.Add(dto);
            }
        }

        private void dgvItens_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 2)
            {
                PedidoDTO dto = dgvItens.CurrentRow.DataBoundItem as PedidoDTO;
                DialogResult resposta = MessageBox.Show("Deseja excluir um produto?",
                                                "CUIDADO!",
                                                MessageBoxButtons.YesNo,
                                                MessageBoxIcon.Question);

                if (resposta == DialogResult.Yes)
                {
                    PedidoBusiness db = new PedidoBusiness();
                    db.Remover(dto.ID);
                }
            }
        }
    }
}
