﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nsf._2018.Modulo3.App.Telas.Login
{
    public partial class frmTrocarSenha2 : UserControl
    {
        public frmTrocarSenha2()
        {
            InitializeComponent();
        }

        private void btnvoltar_Click(object sender, EventArgs e)
        {
            frmTrocarSenha tela = new frmTrocarSenha();
            frmMenu.Atual.AbrirTela(tela);
        }
    }
}
