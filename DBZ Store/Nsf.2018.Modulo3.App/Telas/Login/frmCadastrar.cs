﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Nsf._2018.Modulo3.App.DB.Login;
using System.Text.RegularExpressions;
using Nsf._2018.Modulo3.App.DB;

namespace Nsf._2018.Modulo3.App.Telas
{
    public partial class frmCadastrar : UserControl
    {
        public frmCadastrar()
        {
            InitializeComponent();
        }

        public void Cadastro ()
        {
            try
            {

                DTO_Login dto = new DTO_Login();
                dto.Usuario = txtNomeUsuario.Text;

                //expressão regular(Email)
                string re = @"^[a-zA-Z0-9][a-zA-Z0-9\._-]+@([a-zA-Z0-9\._-]+\.)[a-zA-Z-0-9]{2,3}$";
                Regex regex = new Regex(re);
                if (regex.IsMatch(txtEmail.Text) == false)
                {
                    MessageBox.Show("E-mail invalido",
                                    "e-mail",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Information);
                    return;
                }
                else
                {
                    dto.Email = txtEmail.Text;

                }

                dto.CPF = txtCPF.Text;

                //Caso as senhas não sejam as mesmas
                if (txtSenha.Text != txtConfirmarSenha.Text)
                {
                    MessageBox.Show("As senhas não são iguais certifiquesse de digitar a mesma senhas nos dois campos",
                                    "Senhas",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Error);
                    //Código bonitinho estilo Marcos
                }
                else
                {
                    SHA256Cript crip = new SHA256Cript();
                    dto.Senha = crip.Criptografar(txtSenha.Text);
                }



                Business_Login db = new Business_Login();
                db.Salvar(dto);
                MessageBox.Show("Cadastro efetuado com sucesso!!",
                                "Sucesso!!!",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Information);
            }
            catch (ArgumentException v)
            {
                MessageBox.Show(v.Message,
                                "Cuidado!!",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Information);
            }
            //É aqui que o truque começa!!!
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Cadastro();
            //É aqui que o truque termina!!!
        }


        private void txtConfirmarSenha_KeyDown(object sender, KeyEventArgs e)
        {
            //Consegue perceber quando chamo a variável "e" que representa o tipo KeyEventArgs?
            if (e.KeyCode == Keys.Enter)
            {
                Cadastro();
                //É aqui que o truque termina²!!!
            }

        }

        private void btnvoltar_Click(object sender, EventArgs e)
        {
            frmLogar log = new frmLogar();
            log.Show();
        }
    }
}
