﻿using Nsf._2018.Modulo3.App.Telas.Login;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Net.Mime.MediaTypeNames;

namespace Nsf._2018.Modulo3.App
{
    public partial class frmMenu : Form
    {
        public static frmMenu Atual { get; set; }


        public void AbrirTela(UserControl tela)

        {
            if (panelConteudo.Controls.Count > 0)
                panelConteudo.Controls.RemoveAt(0);

            panelConteudo.Controls.Add(tela);
        }

        public frmMenu()
        {
            InitializeComponent();
            Atual = this;
           
        }

       public Boolean telalogin { get; set; }

       private void cadastrarToolStripMenuItem_Click(object sender, EventArgs e)
        {
              Telas.frmProdutoCadastrar tela = new Telas.frmProdutoCadastrar();
                          AbrirTela(tela);
        }

        private void consultarToolStripMenuItem_Click(object sender, EventArgs e)
        {
                  Telas.frmProdutoConsultar tela = new Telas.frmProdutoConsultar();
                  AbrirTela(tela);
        }

        private void cadastrarToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Telas.frmPedidoCadastrar tela = new Telas.frmPedidoCadastrar();
                        AbrirTela(tela);
        }

        private void consultarToolStripMenuItem1_Click(object sender, EventArgs e)
        {
              Telas.frmPedidoConsultar tela = new Telas.frmPedidoConsultar();
                          AbrirTela(tela);
        }
        private void cadastrarToolStripMenuItem2_Click(object sender, EventArgs e)
        {
                //Telas.Login.frmLogin tela = new Telas.Login.frmLogin();
                //AbrirTela(tela);
        }

        private void alterarSenhaToolStripMenuItem_Click(object sender, EventArgs e)
        {
                Telas.Login.frmTrocarSenha tela = new Telas.Login.frmTrocarSenha();
                AbrirTela(tela);
        }

        private void cadastrarToolStripMenuItem3_Click(object sender, EventArgs e)
        {
                Telas.Login.frmCadastrar tela = new Telas.Login.frmCadastrar();
                AbrirTela(tela);
        }

        private void label1_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void trocarSenhaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Telas.Login.frmTrocarSenha tela = new Telas.Login.frmTrocarSenha();
            AbrirTela(tela);
        }
    }
}
