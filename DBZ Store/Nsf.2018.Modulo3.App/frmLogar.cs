﻿using Nsf._2018.Modulo3.App.DB;
using Nsf._2018.Modulo3.App.DB.Login;
using Nsf._2018.Modulo3.App.Telas.Login;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nsf._2018.Modulo3.App
{
    public partial class frmLogar : Form
    {

        public static frmLogar Logar { get; set; }

        public void OpenScreen(UserControl tela)

        {
            if (pnlConteudo.Controls.Count > 0)
                pnlConteudo.Controls.RemoveAt(0);

            pnlConteudo.Controls.Add(tela);
        }


        public frmLogar()
        {
            InitializeComponent();
            Logar = this;
        }
        public void Login()
        {
            {
                string usuario = txtLogin.Text;
                string senhaantes = txtSenha.Text;

                SHA256Cript crip = new SHA256Cript();
                string senha = crip.Criptografar(senhaantes);

                Business_Login db = new Business_Login();

                bool logou = db.Logar(usuario, senha);

                if (logou == true)
                {
                    frmMenu m = new frmMenu();
                    m.Show();
                    MessageBox.Show("Seja bem vindo!!",
                                "Funfando!!",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Information);
                    this.Hide();
                }
                else
                {
                    MessageBox.Show("Credênciais inválidas!!",
                                    "ERRO!!",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Error);

                    DialogResult r = MessageBox.Show("Deseja fazer um cadastro caso não possua",
                                                 "Master!!",
                                                 MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                    if (r == DialogResult.Yes)
                    {
                        frmCadastrar tela = new frmCadastrar();

                        Logar.OpenScreen(tela);
                        
                    }
                    this.Hide();
                }

            }
        }
                
        private void lblFechar_Click(object sender, EventArgs e)
        {
            DialogResult dialog = MessageBox.Show("Tem certeza que deseja fechar o programa?", 
                                                  "OnePieceStore",
                                                  MessageBoxButtons.YesNo,
                                                  MessageBoxIcon.Question);
            if (dialog == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void txtSenha_KeyDown_1(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Login();
            }
        }

        private void btnEntrar_Click(object sender, EventArgs e)
        {
            Login();
        }
        
        private void lblEsqueceuSenha_Click(object sender, EventArgs e)
        {

            frmTrocarSenha tela = new frmTrocarSenha();
            Logar.OpenScreen(tela);
        }

        private void lblCadastro_Click(object sender, EventArgs e)
        {
            Telas.frmCadastrar tela = new Telas.frmCadastrar();
            Logar.OpenScreen(tela);
        }
    }
}
